﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    [SerializeField] private int life=100;
    // Start is called before the first frame update
    public delegate void DG1(int a);
        
    public void AddScore(int score)
    {
        score += score;
    }

    public int GetLife()
    {
        return life;
    }

    public void SubtractLife(int subtract)
    {
        life -= subtract;
    }

}
