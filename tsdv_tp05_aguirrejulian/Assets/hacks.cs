﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hacks : MonoBehaviour
{
    [Tooltip("C create box, B create bomb")]
    public GameObject box;
    [Tooltip("C create box, B create bomb")]
    public GameObject bomb;
    [Tooltip("V create amo")]
    public GameObject amo;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            Caja(box);
        }
        if (Input.GetKeyDown(KeyCode.B))
        {
            Caja(bomb);
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            Caja(amo);
        }
    }
    void Caja(GameObject a)
    {
        if (Camera.main != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (!Physics.Raycast(ray, out hit, 1000)) return;
            Instantiate(a, hit.point, Quaternion.identity);
        }
    }
}
