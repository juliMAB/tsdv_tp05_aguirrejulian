﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ManagerCanvas : MonoBehaviour
{
    public TextMeshProUGUI text1;
    public TextMeshProUGUI text2;
    public TextMeshProUGUI text3;
    // Start is called before the first frame update
    void Start()
    {
        text1.text = "Score: " + DataSave.Data.GetScore();
        text2.text = "usedBullets: " + DataSave.Data.GetUsedBullets();
        text3.text = "enemiesKilled: " + DataSave.Data.GetEnemiesKilled();
    }
}
