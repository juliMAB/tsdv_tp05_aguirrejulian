﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSave : MonoBehaviour
{
    public static DataSave Data;
    private int enemiesKilled;
    private int usedBullets;
    private int score;

    private void Awake()
    {
        if (DataSave.Data == null)
        {
            DataSave.Data = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void AddScore(int score)
    {
        this.score += score;
    }
    public void AddUsedBullets()
    {
        usedBullets++;
    }
    public void AddKilledEnemies()
    {
        enemiesKilled++;
    }

    public int GetScore()
    {
        return score;
    }

    public int GetEnemiesKilled()
    {
        return enemiesKilled;
    }

    public int GetUsedBullets()
    {
        return usedBullets;
    }
    public void ResetProgress()
    {
        enemiesKilled = 0;
        usedBullets = 0;
        score = 0;
    }
}
