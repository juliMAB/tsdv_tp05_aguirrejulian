﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombStates : MonoBehaviour
{
    public enum EnemyState
    {
        Idle, //0
        ChargeExplotion, //1
        Deathing, //2
        Last,
    }

    [SerializeField] private EnemyState state = EnemyState.Idle;

    public float distanceToActive = 7;
    public float timeToExplote=5;
    public Transform target;

    private float t;
    private Animator animator;
    private ParticleSystem particleSystem1;
    private MeshRenderer meshRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        target = GameObject.FindWithTag("Player").transform;
        particleSystem1 = GetComponentInChildren<ParticleSystem>();
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        t += Time.deltaTime;
        switch (state)
        {
            case EnemyState.Idle:
                if (Vector3.Distance(target.position,transform.position)<distanceToActive)
                {
                    NextState();
                }
                break;
            case EnemyState.ChargeExplotion:
                timeToExplote -= Time.deltaTime;

                animator.Play("BombCharging");
                if (timeToExplote<0)
                {
                    if (meshRenderer)
                    {
                        Destroy(meshRenderer);
                    }
                    particleSystem1.Play();
                    NextState();
                }
                    
                break;
            case EnemyState.Deathing:
                if (!particleSystem1.isPlaying)
                {
                    Die();
                }
                    
                break;
        }
    }

    private void NextState()
    {
        t = 0;
        int intState = (int)state;
        intState++;
        intState = intState % ((int)EnemyState.Last);
        SetState((EnemyState)intState);
    }

    private void SetState(EnemyState es)
    {
        state = es;
    }

    void Die()
    {
        Destroy(gameObject);
    }
}
