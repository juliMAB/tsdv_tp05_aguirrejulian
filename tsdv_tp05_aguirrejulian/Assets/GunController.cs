﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GunController : MonoBehaviour
{
    public enum Weapon
    {
        Pistol,
        Bazooka
    }
    [SerializeField] private Weapon weapon = Weapon.Pistol;
    [SerializeField] public int bullets { get; set; } = 0;
    [SerializeField] public int charger  = 3;
    [SerializeField] public int maxBalas =10;
    [SerializeField] public int bazookaForce = 10;
    [SerializeField] private float range;
    private Camera cam;
    [SerializeField] private GameObject hole;
    [SerializeField] private GameObject bullet;
    [SerializeField] private AudioClip[] audios;
    private new AudioSource audio;
    private Animator animator;
    private Transform cañon;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        animator = GetComponent<Animator>();
        cam = Camera.main;
        cañon = GetComponentInChildren<Transform>();
    }
    public void AddCharge()
    {
        charger++;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            weapon = Weapon.Pistol;
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            weapon = Weapon.Bazooka;
        }
        if (Input.GetMouseButtonDown(0))
        {
            switch (weapon)
            {
                case Weapon.Pistol:
                    if (bullets > 0)
                    {
                        Shoot();
                    }
                    else
                    {
                        animator.Play("NoAmo");
                    }
                    break;
                case Weapon.Bazooka:
                    ShootBazooka();
                    break;
            }

        }

        switch (weapon)
        {
            case Weapon.Pistol:
                if (Input.GetKeyDown(KeyCode.R))
                {
                    if (charger > 0)
                    {
                        Reload();
                    }
                }
                break;
            default:
                break;
        }
    }
    private void ShootBazooka()
    {
        int a = Random.Range(0, audios.Length);
        audio.clip = audios[a];
        audio.Play();
        GameObject go = Instantiate(bullet, cañon.transform.position, GetComponentInParent<PlayerManager>().transform.rotation);
        go.GetComponent<Rigidbody>().velocity = GetComponentInParent<CharacterController>().GetComponentInChildren<AudioListener>().transform.forward * bazookaForce;   



    }
    private void Shoot()
    {
        int a = Random.Range(0, audios.Length);
        audio.clip = audios[a];
        audio.Play();
        
        animator.Play("shoot");
        bullets--;
        DataSave.Data.AddUsedBullets();
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, range)) return;
        if (!hit.transform.GetComponent<Enemy>())
        {
            if (hole == null) return;
            GameObject go = Instantiate(hole, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal));
            Destroy(go, 5);
            return;
        }
        else
        {
            if (hit.transform.CompareTag("BOMB"))
            {
                hit.transform.GetComponent<Enemy>().getdamage(1);
            }
        }
    }

    void Reload()
    {
        animator.Play("reload");
        bullets = maxBalas;
        charger--;
    }

}
