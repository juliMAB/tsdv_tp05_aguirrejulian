﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obtainable : MonoBehaviour
{
    public void death()
    {
        Destroy(gameObject);

    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("Player"))
        {
            if (CompareTag("BOX"))
            {
                DataSave.Data.AddScore(20);
                death();
            }
            if (CompareTag("AMO"))
            {
                other.GetComponentInChildren<GunController>().AddCharge();
                death();
            }
        }
    }
}
