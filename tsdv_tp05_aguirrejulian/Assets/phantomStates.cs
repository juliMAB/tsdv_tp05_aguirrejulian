﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class phantomStates : MonoBehaviour
{
    public enum EnemyState
    {
        Idle, //0
        walking, //1
        follow, //2
        Last,
    }
    private Terrain WorldTerrain;
    private LayerMask TerrainLayer;
    [SerializeField] private EnemyState state = EnemyState.Idle;

    public float waiting = 5;
    public float speed = 2;
    public float distanceToStop = 2;
    public float distanceToRestart = 10;
    public float distanceToActive = 7;
    public Transform target;

    private Vector3 placeToGo;

    private void Start()
    {
        target = GameObject.FindWithTag("Player").transform;
        WorldTerrain = GameObject.FindGameObjectWithTag("terrain").GetComponent<Terrain>();
        TerrainLayer = new LayerMask();
        TerrainLayer = 256;
    }

    private void Update()
    {
        Vector3 dir;
        switch (state)
        {
            case EnemyState.Idle:
                waiting -= Time.deltaTime;
                if (waiting<0)
                {
                    placeToGo = RandvecInTerrain(2);
                    waiting = 5;
                    NextState();
                }

                if (Vector3.Distance(target.position,transform.position) <distanceToActive)
                {
                    placeToGo = target.position;
                    SetState(EnemyState.follow);
                }
                break;
            case EnemyState.walking:
                //transform.Translate(placeToGo.normalized * speed * Time.deltaTime);
                dir = (placeToGo - transform.position).normalized;
                transform.position += dir * speed * Time.deltaTime;
                
                if (Vector3.Distance(placeToGo, transform.position) < 0.1f)
                        SetState(EnemyState.Idle);
                
                
                    if (Vector3.Distance(target.position, transform.position) < distanceToActive)
                    {
                        placeToGo = target.position;
                        SetState(EnemyState.follow);
                    }
                    break;
            case EnemyState.follow:
                dir = (target.position - transform.position).normalized;
                transform.position += dir * speed * Time.deltaTime;
                if (Vector3.Distance(target.position, transform.position) > distanceToRestart)
                {
                    SetState(EnemyState.Idle);
                }
                break;
        }
    }

    private void NextState()
    {
        int intState = (int)state;
        intState++;
        intState = intState % ((int)EnemyState.Last);
        SetState((EnemyState)intState);
    }

    private void SetState(EnemyState es)
    {
        state = es;
    }
    Vector3 RandvecInTerrain(float addHigh)
    {
        float TerrainLeft = WorldTerrain.transform.position.x;
        float TerrainBottom = WorldTerrain.transform.position.z;
        float TerrainWidth = WorldTerrain.terrainData.size.x;
        float TerrainLength = WorldTerrain.terrainData.size.z;
        float TerrainRigth = TerrainLeft + TerrainWidth;
        float TerrainTop = TerrainBottom + TerrainLength;
        RaycastHit hit;
        float randomPositionX = UnityEngine.Random.Range(TerrainLeft, TerrainRigth);
        float randomPositionZ = UnityEngine.Random.Range(TerrainBottom, TerrainTop);
        float randomPositionY = 0f;

        if (Physics.Raycast(new Vector3(randomPositionX, 9999f, randomPositionZ), Vector3.down, out hit, Mathf.Infinity, TerrainLayer))
        {
            randomPositionY = hit.point.y + addHigh;
        }

        return new Vector3(randomPositionX, randomPositionY, randomPositionZ);
    }
    void Die()
    {
        Destroy(gameObject);
    }
}
