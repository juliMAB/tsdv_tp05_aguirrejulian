﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour
{
    public void GoMenu()
    {
        MySceneManager.inst.GoToMenu();
    }

    public void GoGame()
    {
        MySceneManager.inst.GoToGame();
    }
    public void GoGameOver()
    {
        MySceneManager.inst.GoToGameOver();
    }

    public void ExitGame()
    {
        MySceneManager.inst.ExitGame();
    }
}
