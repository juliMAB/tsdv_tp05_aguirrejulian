﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MySceneManager : MonoBehaviour
{
    public static MySceneManager inst;
    private void Awake()
    {
        if (MySceneManager.inst == null)
        {
            MySceneManager.inst = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void GoToGame()
    {
        SceneManager.LoadScene("GAME");

    }

    public void ExitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
    public void GoToMenu()
    {
        SceneManager.LoadScene("MENU");
    }

    public void GoToGameOver()
    {

        SceneManager.LoadScene("END");
    }
}
