﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    [SerializeField] private GameObject target;

    private void Awake()
    {
        target = GameObject.FindWithTag("Player");
    }
    void Update()
    {
        transform.LookAt(target.transform.position);
    }
}
