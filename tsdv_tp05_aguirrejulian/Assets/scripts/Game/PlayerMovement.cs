﻿using System;
using System.Collections;
using System.Collections.Generic;
using Homebrew;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
   

    [Foldout("MANAGER",true)]
    public CharacterController controller;

    public int score;

    public bool actualizateRequered = false;

    public int scorePerBox;

    [Foldout("PLAYER", true)]
    public int life = 100;

    public float speed = 12f;

    public float gravity = -9.81f;

    public float jumpHeight = 3f;

    private Vector3 velocity;

    private bool checkCol;

    public float mouseSensitivity = 100f;
    [Foldout("PISTOL", true)]
    public int balas;

    [SerializeField] public int charger = 3;
    [SerializeField] public int maxBalas;

    [SerializeField] public int rangeDistancia=100;

    [Foldout("ADDS", true)]

    [SerializeField] private GameObject objetoAInstanciar = null;

    [SerializeField] private float timeToDestroy;

    [Foldout("INFO", true)]

    [NonSerialized] private int usedBullets;

    [NonSerialized] private int enemiesKilled;

    [Foldout("CAMERA", true)]

    [SerializeField] private Camera cam;

    private Ray ray;

    private float xRotation = 0f;

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    void Update()
    {
        UpdateCamera();
        UpdateRay();
        Jump();
        MovePj();
        AplicateGravity();
        Click();
        Reload();
        ShowWeapon();
    }
    void UpdateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        cam.transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
        transform.Rotate(Vector3.up * mouseX);
    }
    void UpdateRay()
    {
        ray = cam.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(ray.origin, ray.direction * 50, Color.yellow);
    }
    private void Click()
    {
        if (!Input.GetMouseButtonDown(0)) return;
        if (balas == 0)
        {
            GetComponentInChildren<Animation>().Play("NoAmo");
            return;
        }
        balas--;
        usedBullets++;
        cam.GetComponentInChildren<Animation>().Play("shoot");
        RaycastHit hit;
        actualizateRequered = true;
        if (!Physics.Raycast(ray, out hit, rangeDistancia)) return;
        if (hit.transform.CompareTag("Untagged"))
        {
            if (objetoAInstanciar == null) return;
            GameObject go = Instantiate(objetoAInstanciar, hit.point + hit.normal * 0.001f,
                Quaternion.LookRotation(hit.normal));
            Destroy(go, timeToDestroy);
            actualizateRequered = true;
        }

        if (hit.transform.parent!=null)
        {
            if (hit.transform.parent.CompareTag("Enemy"))
            {
                if (!hit.transform.CompareTag("BOMB")) return;
                Destroy(hit.transform.gameObject);
                score += 100;
                actualizateRequered = true;
                enemiesKilled++;
            }
        }
    }
    void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (charger>0)
            {
                charger--;
                GetComponentInChildren<Animation>().Play("reload");
                balas = maxBalas;
                actualizateRequered = true;
            }
            
        }
    }
    void Jump()
    {
        if (controller.isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
            if (Input.GetButtonDown("Jump"))
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }
        }
    }

    public int GetScore()
    {
        return score;
    }
    public int GetEnemiesKilled()
    {
        return enemiesKilled;
    }

    public int GetBulletsUsed()
    {
        return usedBullets;
    }

    public bool YouRDead()
    {
        return life <= 0;
    }
    void MovePj()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);
    }
    void ShowWeapon()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            GetComponentInChildren<Animation>().Play("show");
        }
        
    }
    void AplicateGravity()
    {
        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
    public void TakeDamage(int d)
    {
        life -= d;
    }

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (!checkCol)
        {
            if (hit.transform.parent != null)
            {
                if (hit.transform.parent.CompareTag("Enemy"))
                {
                    if (hit.transform.CompareTag("BOMB"))
                    {

                        //TakeDamage(hit.gameObject.GetComponent<Enemy>().Atack());
                        controller.Move(Vector3.Normalize(transform.position - hit.transform.position) * 2);
                        Destroy(hit.gameObject);
                        actualizateRequered = true;
                        checkCol = true;
                    }
                    if (hit.transform.CompareTag("BOX"))
                    {
                        Destroy(hit.gameObject);
                        score += scorePerBox;
                        actualizateRequered = true;
                        checkCol = true;
                    }
                }
            }
        }
        if (hit.transform.CompareTag("Untagged"))
        {
            checkCol = false;
        }
    }
}
