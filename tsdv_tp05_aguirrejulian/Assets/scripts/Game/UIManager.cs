﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Homebrew;

public class UIManager : MonoBehaviour
{
    [Foldout("text")]
    [SerializeField] private TextMeshProUGUI textLeftTop;
    [Foldout("text")]
    [SerializeField] private TextMeshProUGUI textRightTop;

    [SerializeField] private GunController gun;

    [SerializeField] private PlayerManager player;
    [SerializeField] private DataSave data;


    public bool hint=false;

    private void Start()
    {
        textLeftTop.text = "H to hint";
        textRightTop.text = "Vida:" + player.GetLife() + "%" + "\nScore:" + DataSave.Data.GetScore() + "\nBalas:" + gun.bullets + "/" + gun.maxBalas + "\n Charges: " +
                            gun.charger + "\n ";
    }

    private void Update()
    {
        HintUpdate();
        LogUpdate();
    }

    public void LogUpdate()
    {
        textRightTop.text = "Vida:" + player.GetLife() + "%" + "\nScore:" + DataSave.Data.GetScore() + "\nBalas:" + gun.bullets + "/" + gun.maxBalas + "\n Charges: " +
                            gun.charger + "\n ";
    }
    private void HintUpdate()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            hint = !hint;
            if (hint)
            {
                textLeftTop.text = "Move whit WASD\n" +
                                   "shot whit leftClick\n"+
                                   "R reload\n"+
                                   "The render on the bomb change if you are enough close to shot";

                var color = textLeftTop.color;
                color.a = 1;
                textLeftTop.color = color;
            }
            else
            {
                textLeftTop.text = "H to hint";
                var color = textLeftTop.color;
                color.a = 0.5f;
                textLeftTop.color = color;
            }
        }
    }
}
