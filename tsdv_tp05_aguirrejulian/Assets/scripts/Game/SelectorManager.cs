﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectorManager : MonoBehaviour
{
    [SerializeField] private string selectableTag = "Selectable";
    [SerializeField] private Material highlightMaterial;
    [SerializeField] private Material defaultMaterial;
    [SerializeField] private float distance;
    private Transform _selection;

    private void Update()
    {
        if (_selection!=null)
        {
            var selectionRender = _selection.GetComponent<Renderer>();
            if (selectionRender)
            {
                selectionRender.material = defaultMaterial;
            }
            _selection = null;
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray,out hit,distance))
        {
            var selection = hit.transform;
            if (selection.CompareTag(selectableTag))
            {
                var selectionRender = selection.GetComponent<Renderer>();
                if (selectionRender != null)
                {
                    selectionRender.material = highlightMaterial;
                }
                _selection = selection;
            }
        }
    }
}
