﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Terrain WorldTerrain;
    [SerializeField] private LayerMask TerrainLayer;
    private float TerrainLeft, TerrainRigth, TerrainTop, TerrainBottom, TerrainWidth, TerrainLength, TerrainHight;
    [SerializeField] private GameObject bomb;
    [SerializeField] private GameObject box;
    [SerializeField] private GameObject phantom;
    [SerializeField] private float timeToSpawnBOMB = 10;
    [SerializeField] private float timeToSpawnBOX = 20;
    [SerializeField] private float timeToSpawnphantom = 5;
    [SerializeField] private GameObject Enemys;

    private void Awake()
    {
        TerrainLeft = WorldTerrain.transform.position.x;
        TerrainBottom = WorldTerrain.transform.position.z;
        TerrainWidth = WorldTerrain.terrainData.size.x;
        TerrainLength = WorldTerrain.terrainData.size.z;
        TerrainHight = WorldTerrain.terrainData.size.y;
        TerrainRigth = TerrainLeft + TerrainWidth;
        TerrainTop = TerrainBottom + TerrainLength;
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timeToSpawnBOMB -= Time.deltaTime;
        timeToSpawnBOX -= Time.deltaTime;
        timeToSpawnphantom -= Time.deltaTime;
        if (timeToSpawnBOMB<=0)
        {
            SpawnRandom(bomb);
            timeToSpawnBOMB = 10;
        }

        if (timeToSpawnBOX<=0)
        {
            SpawnRandom(box);
            timeToSpawnBOX = 20;
        }

        if (timeToSpawnphantom<=0)
        {
            SpawnRandom(phantom);
            timeToSpawnphantom = 5;
        }
    }
    void SpawnRandom(GameObject a)
    {
        GameObject go = Instantiate(a, Vector3.zero, Quaternion.identity);
        go.transform.position = RandvecInTerrain(bomb.transform.localScale.y / 2);
        go.transform.parent = Enemys.transform;
    } 
    Vector3 RandvecInTerrain(float addHigh)
    {
        RaycastHit hit;
        float randomPositionX = UnityEngine.Random.Range(TerrainLeft, TerrainRigth);
        float randomPositionZ = UnityEngine.Random.Range(TerrainBottom, TerrainTop);
        float randomPositionY = 0f;

        if (Physics.Raycast(new Vector3(randomPositionX, 9999f, randomPositionZ), Vector3.down, out hit, Mathf.Infinity, TerrainLayer))
        {
            randomPositionY = hit.point.y + addHigh;
        }

        return new Vector3(randomPositionX, randomPositionY, randomPositionZ);
    }
    
}
