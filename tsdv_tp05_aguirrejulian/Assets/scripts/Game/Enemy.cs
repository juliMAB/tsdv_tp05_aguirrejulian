﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private int life;
    [SerializeField] private int ScoreGive;
    [SerializeField] private bool bazooka;

    public void getdamage(int reciveDamage)
    {
        life -= reciveDamage;
    }

    private void Update()
    {
        if (life < 0)
        {
            DataSave.Data.AddScore(ScoreGive);
            DataSave.Data.AddKilledEnemies();
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerManager>().SubtractLife(damage);
            if (CompareTag("BOMB"))
            {
                Destroy(gameObject);
            }
        }
    }
}

