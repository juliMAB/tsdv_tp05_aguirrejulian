﻿using System;
using System.Collections;
using System.Collections.Generic;
using Homebrew;
using UnityEngine;
using UnityEngine.UIElements;
using Cursor = UnityEngine.Cursor;
using Random = System.Random;

public class GameManager : MonoBehaviour
{
    public enum GAME
    {
        In,
        Over,
        Count
    }
    [SerializeField] public PlayerMovement pj;
    [Foldout("MANAGER", true)]
    //minimo radio de spawn entre objetos.
    [SerializeField] private float radioObjets;
    //maximo de objetos creados.
    [SerializeField] private int objsMaxCuantity;

    [NonSerialized] public GAME StateGame;
    private float errorDetector=0;
    [Foldout("TERRAIN", true)]
    [SerializeField] private Terrain WorldTerrain;
    [SerializeField] private LayerMask TerrainLayer;
    private float TerrainLeft, TerrainRigth, TerrainTop, TerrainBottom, TerrainWidth, TerrainLength, TerrainHight;
    [Foldout("SPAWNER", true)]
    [SerializeField] private Transform player;
    [SerializeField] private float range;
    [SerializeField] private bool spawn;
    [SerializeField] private GameObject Enemys;
    [SerializeField] public GameObject bomb;
    [SerializeField] public GameObject box;
    [SerializeField] public float timeToSpawnBOMB = 10;

    [Foldout("SPAWNER")]
    [SerializeField] public float timeToSpawnBOX = 20;
    public List<Collider> objsList;

    private void AwakeTerrain()
    {
        TerrainLeft = WorldTerrain.transform.position.x;
        TerrainBottom = WorldTerrain.transform.position.z;
        TerrainWidth = WorldTerrain.terrainData.size.x;
        TerrainLength = WorldTerrain.terrainData.size.z;
        TerrainHight = WorldTerrain.terrainData.size.y;
        TerrainRigth = TerrainLeft + TerrainWidth;
        TerrainTop = TerrainBottom + TerrainLength;
    }
    private void Awake()
    {
       AwakeTerrain();
    }
   
    public void InstanciateRandomPosition(GameManager a, int amount, float addedHeight)
    {
        var i = 0;
        float terrainHeight = 0f;
        RaycastHit hit;
        Vector3 randomPosition = Vector3.zero;
        do
        {
            i++;
            float randomPositionX = UnityEngine.Random.Range(TerrainLeft, TerrainRigth);
            float randomPositionZ = UnityEngine.Random.Range(TerrainBottom, TerrainTop);

            if (Physics.Raycast(new Vector3(randomPositionX,9999f,randomPositionZ),Vector3.down,out hit,Mathf.Infinity,TerrainLayer))
            {
                terrainHeight = hit.point.y;
            }

            float randomPositionY = terrainHeight + addedHeight;

            randomPosition = new Vector3(randomPositionX, randomPositionY, randomPositionZ);

            Instantiate(a, randomPosition, Quaternion.identity);
        } while (i<amount);

    }
    void Start()
    {
        SpawnRandom();
    }

    public bool End()
    {
        if (pj.life<=0)
        {
            Cursor.lockState = CursorLockMode.None;
            MySceneManager.inst.GoToGameOver();
            return true;
        }

        return false;
    }

    public bool GameOver()
    {
        return StateGame == GAME.Over;
    }

    void SpawnRandom2()
    {
        GameObject go = Instantiate(bomb, Vector3.zero, Quaternion.identity);
        go.transform.position = RandvecInTerrain(bomb.transform.localScale.y / 2);
    }
    void SpawnRandom()
    {
        GameObject go = Instantiate(bomb, Vector3.zero, Quaternion.identity);
        do
        {
            go.transform.position = RandvecInTerrain(bomb.transform.localScale.y/2);
            BugAvoider();
        } while (Chocan(player.GetComponent<Collider>(), go.GetComponent<Collider>()));
        errorDetector = 0;
        go.transform.parent = Enemys.transform;
        Collider goCollider = go.GetComponent<Collider>();
        objsList.Add(goCollider);
    }
    Vector3 RandvecInTerrain(float addHigh)
    {
        RaycastHit hit;
        float randomPositionX = UnityEngine.Random.Range(TerrainLeft, TerrainRigth);
        float randomPositionZ = UnityEngine.Random.Range(TerrainBottom, TerrainTop);
        float randomPositionY = 0f;

        if (Physics.Raycast(new Vector3(randomPositionX, 9999f, randomPositionZ), Vector3.down, out hit, Mathf.Infinity, TerrainLayer))
        {
            randomPositionY = hit.point.y+addHigh;
        }

        return new Vector3(randomPositionX, randomPositionY, randomPositionZ);
    }

    Vector3 RandvecInRange()
    {
        var position = player.transform.position;
        return new Vector3(UnityEngine.Random.Range(position.x - range, position.x + range),
            bomb.transform.localScale.x / 2,
            UnityEngine.Random.Range(position.z - range, position.z + range));
    }
    void BugAvoider()
    {
        //si en algun momento queda atrapado en el bucle que destruya todos los objetos. Solucion momentanea.
        errorDetector += Time.deltaTime;
        if (errorDetector>10)
        {
            ClearStage();
        }
    }
    void ClearStage()
    {
        for (var i = 0; i < objsList.Count; i++)
        {
            GameObject go = objsList[i].gameObject;
            objsList.RemoveAt(i);
            Destroy(go);
        }
    }
    void Update()
    {
        Spawn();
        End();
    }
    void Spawn()
    {
        if (!spawn) return;
        if (objsMaxCuantity <= objsList.Count) return;
        SpawnBox();
        SpawnBomb();
    }
    void SpawnBox()
    {
        timeToSpawnBOX += Time.deltaTime;
        if (timeToSpawnBOX < 20) return;
        timeToSpawnBOX = 0;
        GameObject go = Instantiate(box, Vector3.zero, Quaternion.identity);
        do
        {
            do
            {
                go.transform.position = RandvecInTerrain(box.transform.localScale.y / 2);
                BugAvoider();
            } while (ChocanList(go.GetComponent<Collider>()));
        } while (Chocan(player.GetComponent<Collider>(), go.GetComponent<Collider>()));
        errorDetector = 0;

        go.transform.parent = Enemys.transform;
        Collider goCollider = go.GetComponent<Collider>();
        objsList.Add(goCollider);
    }
    void SpawnBomb()
    {
        timeToSpawnBOMB += Time.deltaTime;
        if (timeToSpawnBOMB < 10) return;
        timeToSpawnBOMB = 0;
        GameObject go = Instantiate(bomb, Vector3.zero, Quaternion.identity);
        do
        {
            do
            {
                go.transform.position = RandvecInTerrain(bomb.transform.localScale.y / 2);
                BugAvoider();
            } while (ChocanList(go.GetComponent<Collider>()));
        } while (Chocan(player.GetComponent<Collider>(), go.GetComponent<Collider>()));
        errorDetector = 0;

        go.transform.parent = Enemys.transform;
        Collider goCollider = go.GetComponent<Collider>();
        objsList.Add(goCollider);
    }
    
    bool Chocan(Collider a, Collider b)
    {
        Vector3 puntoClosestPointA = a.ClosestPoint(b.transform.position);
        Vector3 puntoClosestPointB = b.ClosestPoint(puntoClosestPointA);
        return (Vector3.Distance(puntoClosestPointA, puntoClosestPointB) < radioObjets);
    }
    bool ChocanList(Collider b)
    {
        for (var index = 0; index < objsList.Count; index++)
        {
            var t = objsList[index];
            if (t != null)
            {
                Vector3 puntoClosestPointA = t.ClosestPoint(b.transform.position);
                Vector3 puntoClosestPointB = b.ClosestPoint(puntoClosestPointA);
                if (Vector3.Distance(puntoClosestPointA, puntoClosestPointB) < radioObjets)
                {
                    return true;
                }
            }
            else
            {
                objsList.RemoveAt(index);
            }
        }

        return false;
    }
}
