﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class highscore : MonoBehaviour
{
    public static highscore inst;

    private TextMeshProUGUI pj1;

    private TextMeshProUGUI pj2;

    private TextMeshProUGUI pj3;

    private void FindText()
    {
        if (SceneManager.GetActiveScene().name=="MENU")
        {
            pj1 = GameObject.Find("pj1").GetComponent<TextMeshProUGUI>();
            pj2 = GameObject.Find("pj2").GetComponent<TextMeshProUGUI>();
            pj3 = GameObject.Find("pj3").GetComponent<TextMeshProUGUI>();
        }
    }
    //el 0 va a ser el mas alto.
    private string[] names = new string[3];
    private int[] scores = new int[3];
    private void Awake()
    {
        if (highscore.inst == null)
        {
            highscore.inst = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        //cargalos si existen.
        for (int i = 0; i < 3; i++)
        {
            if (PlayerPrefs.HasKey("name" + i + 1))
            {
                names[i] = PlayerPrefs.GetString("name" + i + 1);
            }

            if (PlayerPrefs.HasKey("score" + i + 1))
            {
                scores[i] = PlayerPrefs.GetInt("score" + i + 1);
            }
           
        }
    }

    private void Start()
    {
        FindText();
    }

    public void AddHightScore(int score, string name)
    {
        for (int i = 2; i >= 0; i--)
        {
            if (score <= scores[i]) continue;
            if (i != 2)
            {
                names [i + 1] = names [i];
                scores[i + 1] = scores[i];
            }
            scores[i]=score;
            names[i] = name;
        }
        
    }
    void SaveHigthScore()
    {
        PlayerPrefs.SetString("name1", names[0]);
        PlayerPrefs.SetString("name2", names[1]);
        PlayerPrefs.SetString("name3", names[2]);
        PlayerPrefs.SetInt("scores1", scores[0]);
        PlayerPrefs.SetInt("scores2", scores[1]);
        PlayerPrefs.SetInt("scores3", scores[2]);
    }
    private void OnDestroy()
    {
        //guardalos.
        SaveHigthScore();
    }
}
