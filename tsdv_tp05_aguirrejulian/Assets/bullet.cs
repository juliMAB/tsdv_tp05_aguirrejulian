﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    [SerializeField] private float force;

    private Vector3 dir;
    // Start is called before the first frame update
    private void Awake()
    {

    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<Enemy>())
        {
            if (!other.gameObject.CompareTag("BOMB"))
            {
                other.gameObject.GetComponent<Enemy>().getdamage(10);
            }
        }
        Destroy(gameObject);
    }
}
